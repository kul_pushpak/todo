import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Priorities } from 'src/app/model/model';

@Component({
  selector: 'td-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  @Input() id: string;
  @Input() description: string;
  @Input() isDone: boolean;


  @Output() update: EventEmitter<UpdateConfig> = new EventEmitter<UpdateConfig>();
  constructor() { }

  ngOnInit(): void {
  }

  private sendUpdate(config) {
    this.update.emit(config);
  }

  onUpdateDescription(value) {
    this.description = value;
    this.updateTaskDetails();
  }

  updateIsDone(val) {
    this.isDone = val;
    this.updateTaskDetails('isDone');
  }

  updateTaskDetails(operation: string = 'update') {
    const data = {
      id: this.id,
      description: this.description,
      isDone: this.isDone
    };

    this.sendUpdate({
      type: operation,
      data
    });
  }
}


interface UpdateConfig {
  type: 'update' | 'delete';
  data?: any;
}
