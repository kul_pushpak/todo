import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskComponent } from './task.component';

describe('TaskComponent', () => {
  let component: TaskComponent;
  let fixture: ComponentFixture<TaskComponent>;
  const id = 'id_1';
  const isDone = false;
  const description = 'Test Text';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaskComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskComponent);
    component = fixture.componentInstance;
    component.id = id;
    component.isDone = isDone;
    component.description = description;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#updateIsDone() should update isDone property', () => {
    component.updateIsDone(true);
    expect(component.isDone).toBeTrue();

    component.updateIsDone(isDone);
    expect(component.isDone).toEqual(isDone);
  });

  it('#onUpdateDescription() should set description property ', () => {
    component.onUpdateDescription(description);
    fixture.detectChanges();
    expect(component.description).toEqual(description);
  });

  it('#updateTaskDetails() should emit update', () => {

    let update;
    let data;
    component.update.subscribe(
      (response) => update = response
    );
    component.updateTaskDetails();

    data = update.data;

    expect(update.type).toEqual('update');
    expect(data.isDone).toEqual(isDone);
    expect(data.id).toEqual(id);
    expect(data.description).toEqual(description);
  });
});
