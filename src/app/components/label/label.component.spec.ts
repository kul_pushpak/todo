import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabelComponent } from './label.component';

describe('LabelComponent', () => {
  let component: LabelComponent;
  let fixture: ComponentFixture<LabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have properties', () => {
    expect(component.label).toBeDefined();
    expect(component.labelChange).toBeDefined();
    expect(component.showLabelInput).toBeDefined();
    expect(component.showLabelInput).toBeFalsy();
  });

  it('#updateLabel() should update value', () => {
    expect(component.label).toEqual('');
    component.updateLabel('test');
    expect(component.label).toEqual('test');
  });

  it('#toggleLabelInput() should toggle boolean value', () => {
    expect(component.showLabelInput).toBeFalse();
    component.toggleLabelInput();
    expect(component.showLabelInput).toBeTrue();
  });
});
