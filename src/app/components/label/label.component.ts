import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'td-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss']
})
export class LabelComponent implements OnInit {

  showLabelInput = false;

  @Input() label = '';

  @Output() labelChange = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    if (!this.label) {
      this.label = '';
    }
  }

  updateLabel(value) {
    this.label = value;
    this.toggleLabelInput();
    this.labelChange.emit(this.label);
  }

  toggleLabelInput() {
    this.showLabelInput = !this.showLabelInput;
  }
}
