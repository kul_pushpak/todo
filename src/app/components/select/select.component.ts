import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'td-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

  showDropdown = false;

  @Input() value: string;

  @Input() options: Array<string>;

  @Output() selectionChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }
  onChange(value) {
    this.value = value;
    this.showDropdown = false;
    this.selectionChange.emit(value);
  }

}
