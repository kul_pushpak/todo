import { Component, EventEmitter, HostBinding, HostListener, Input, OnInit, Output } from '@angular/core';
import { Task } from 'src/app/model/model';

@Component({
  selector: 'td-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  private titleTxt;

  @Input() id: string;
  @Input() set title(val: string) {
    this.titleTxt = val;
  }
  get title() {
    return this.titleTxt;
  }

  @Input() taskList: Array<Task>;

  @Input() priority: string;

  @Input() priorities: Array<string>;

  @Input() label = 'Label';

  @Input() isListSelected = false;

  @Output() updateList: EventEmitter<any> = new EventEmitter<any>();

  showTaskDelete = false;

  private updateTaskIds: Array<any> = [];

  @HostListener('document:keyup', ['$event'])
  handleDeleteKeyboardEvent(event: KeyboardEvent) {
    if (!this.isListSelected) { return; }
    if (event.key === 'Delete') {
      this.deleteTasks();
    }
  }


  constructor() {
    this.taskList = [];
  }

  ngOnInit(): void {
  }

  addNewTask() {
    if (!this.taskList) {
      this.taskList = [];
    }
    this.taskList.push(new Task());
  }

  onTaskUpdate(event) {
    const operation = `${event.type}Task`;
    this[operation](event.data);
  }

  isDoneTask(data) {
    const idx = this.taskList.findIndex(
      (task) => {
        return task.id === data.id;
      }
    );
    this.taskList.splice(idx, 1, data);
    this.IsAnyTaskDone();
  }
  deleteTask(data) {
    const idx = data?.id;
    this.taskList = this.taskList.filter(
      (task) => {
        return task.id !== idx;
      }
    );
    this.emitUpdate();
  }
  updateTask(data) {
    this.updateTaskIds.push(data.id);
    const taskIdx = this.taskList.findIndex(
      (task, idx) => {
        return data.id === task.id;
      }
    );
    this.taskList.splice(taskIdx, 1, data);
    this.emitUpdate();
  }

  onTitleChange(event) {
    this.title = event;
    this.emitUpdate();
  }

  onLabelChange(event) {
    this.label = event;
    this.emitUpdate();
  }

  onPriorityChange(event) {
    this.priority = event;
    this.emitUpdate();
  }

  IsAnyTaskDone() {
    this.showTaskDelete = this.taskList.some(
      (task) => {
        return task.isDone;
      }
    );
  }
  deleteTasks() {
    this.taskList = this.taskList.filter(
      (task) => {
        return !task.isDone;
      }
    );
    this.emitUpdate();
  }

  deleteList() {
    const config = {
      operation: 'delete',
      data: {
        id: this.id
      }
    };
    this.emitUpdate(config);
  }

  emitUpdate(data = null) {
    let config = data;
    if (!config) {
      config = {
        operation: 'update',
        data: {
          id: this.id,
          title: this.title,
          taskList: this.taskList,
          priority: this.priority,
          priorities: this.priorities,
          label: this.label,
        }
      };
    }
    this.updateList.emit(config);
    this.showTaskDelete = false;
  }

}
