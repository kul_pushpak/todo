import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { TaskList } from 'src/app/model/model';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'td-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  selectedTLID: string;
  SelectedTLIdx = -1;

  taskLists: TaskList[] = [
    {
      id: 'dashboard-1',
      title: 'Dashboard 1',
      priority: 'low',
      priorities: [
        'low',
        'medium',
        'high'
      ],
      label: 'Personal',
      tasks: [
        {
          id: 'task_0001',
          description: 'Task 1',
          isDone: false
        },
        {
          id: 'task_0002',
          description: 'Task 2',
          isDone: false
        }
      ]
    }
  ];

  @HostListener('document:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.key === 'Enter' && this.SelectedTLIdx === -1) {
      this.addNewTaskList();
    }
    if (event.key === 'ArrowDown') {
      event.stopPropagation();
      event.stopImmediatePropagation();
      this.selectTaskList(1);
    } else if (event.key === 'ArrowUp') {
      event.stopPropagation();
      event.stopImmediatePropagation();
      this.selectTaskList(-1);
    }
    this.scrollElement(this.document.querySelector('.task-list__select'));
  }

  constructor(
    @Inject(DOCUMENT) private document: Document
  ) { }

  ngOnInit(): void {
  }

  addNewTaskList() {
    this.taskLists.unshift(new TaskList());
  }

  onListUpdate(event) {
    if (!event.operation) {
      return;
    }
    const operation = `${event.operation}List`;
    this[operation](event.data);
  }

  deleteList(data) {
    this.taskLists = this.taskLists.filter(
      (taskList) => {
        return taskList.id !== data.id;
      }
    );
  }
  updateList(data) {
    console.log('Tasklist data updated');
  }
  selectTaskList(num) {
    this.selectedTLID = null;
    this.SelectedTLIdx = this.numberInRange(this.SelectedTLIdx + num, -1, this.taskLists.length - 1);
    this.selectedTLID = this.SelectedTLIdx > -1 && this.taskLists[this.SelectedTLIdx].id;
  }

  scrollElement(el) {
    el?.scrollIntoView({
      behavior: 'smooth',
      block: 'center'
    });
  }
  numberInRange(num, min, max) {
    let no = num;
    if (no <= min) {
      no = min;
    } else if (no >= max) {
      no = max;
    }
    return no;
  }
}
