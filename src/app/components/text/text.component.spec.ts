import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { TextComponent } from './text.component';

describe('TextComponent', () => {
  let component: TextComponent;
  let fixture: ComponentFixture<TextComponent>;
  let textEL: HTMLElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TextComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const textDebugEl = fixture.debugElement.query(By.css('.text > div'));
    textEL = textDebugEl.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize to defaults', () => {
    expect(component.showPlaceholder).toBeTrue();
    expect(component.placeholder).toBeTruthy();
    expect(component.text).toEqual('Enter text...');
    expect(component.textChange).toBeDefined();
    expect(textEL.innerHTML).toEqual(component.placeholder);
  });
});

describe('Text Component input binding', () => {
  let component: TextComponent;
  let fixture: ComponentFixture<TextComponent>;
  let textEL: HTMLElement;
  const testText = 'Test Text';

  beforeEach(() => {
    fixture = TestBed.createComponent(TextComponent);
    component = fixture.componentInstance;
    component.text = testText;
    const textDebugEl = fixture.debugElement.query(By.css('.text > div'));
    textEL = textDebugEl.nativeElement;
    fixture.detectChanges();
  });

  it('should bind text input', () => {
    expect(textEL.innerHTML).toContain(testText);
  });

  it('#onTextChange() should set text to placeholder if text is empty', () => {

    expect(component.text).toEqual(testText);

    component.onTextChange('');

    expect(component.text).toEqual(component.placeholder);
    expect(component.showPlaceholder).toBeTrue();
  });

  it('#onTextChange() should emit text value if not set to empty string', () => {
    const txt = 'Test Text 1';
    component.onTextChange(txt);
    expect(component.text).toEqual(txt);
  });

  it('#onFocus() should clear text when focused ', () => {
    component.showPlaceholder = true;
    component.onFocus();
    expect(component.text).toEqual('');
    expect(component.showPlaceholder).toBeFalse();
  });
});
