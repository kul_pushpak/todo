import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'td-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit {
  showPlaceholder = false;

  @Input() placeholder = 'Enter text...';
  @Input() text: string = null;

  @Output() textChange: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('div') childDiv;

  constructor() { }

  ngOnInit(): void {
    this.showPlaceholder = !this.text?.trim();
    if (this.showPlaceholder) {
      this.text = this.placeholder;
    }
  }

  onTextChange(event) {
    this.text = event;
    if (!this.text.trim()) {
      this.text = this.placeholder;
      this.showPlaceholder = true;
      return;
    }
    this.textChange.emit(this.text);
  }

  onFocus() {
    if (this.showPlaceholder) {
      this.text = '';
      this.showPlaceholder = false;
    }
  }

}
