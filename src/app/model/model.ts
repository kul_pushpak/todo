
export class Task {
    id: string;
    description = ' ';
    isDone = false;

    constructor() {
        this.id = generate();
    }

}
export class TaskList {
    id: string;
    title = ' ';
    tasks: Task[] = [];
    priorities: Array<string> = Priorities;
    priority: string = Priorities[0];
    label: string;
    constructor() {
        this.id = generate('tasklist_');
    }
}
export const Priorities = [
    'low',
    'medium',
    'high'
];
function generate(prefix: string = 'id_') {
    return `${prefix}${Number.parseInt((Math.random() * 100000).toString(), 10)}`;
}

