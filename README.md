# ToDo List

Simple ToDo List to manage multiple ToDo lists with tasks. Set priority of list, give label to list, add or delete tasks and lists. 

## Features
- Set priority and lebel to list
- Add, update or delete tasks from list
- Use down/up arrow key to select todo list
- Muliple tasks can be deleted by checking by pressing delete key

## Development Setup

### Prerequisites

- Install [Node.js](https://nodejs.org/en/) which includes [Node Package Manager](https://www.npmjs.com/)

### Setting Up a Project

Install the Angular CLI globally:

```
npm install -g @angular/cli
```

Clone the repository

Install dependencies
```
npm install
```
##### Run below commands
Serve locally 
``` ng serve ```

Build production version
``` ng build --prod=true ```

Unit Test
``` ng test ```

#### Improvments
- Use of Pub-Sub design pattern for optimization
- Allow user to create own priorities
- Allow user to add multiple tags 
- Feature to add images, video links to task list
- Sharing lists with other users 
- Enable integration with other applications

## License
**Free Software, Yeah!**
